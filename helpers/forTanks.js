// the turn action represents the shortest possible way to turn towards an angle
const getTurnActionTowardsTargetAngle = (myAngle, targetAngle) => {    
    var angleDiff = (myAngle - targetAngle) % 365
    if (Math.abs(angleDiff) < 2) return 'idle' // we're aiming at our target     
    else {
        if (365 > angleDiff && angleDiff >= 180) return 'clockwise'
        else if (180 > angleDiff && angleDiff >= 0) return 'counterclockwise'
        else if (0 > angleDiff && angleDiff >= -180) return 'clockwise'
        else if (-180 > angleDiff && angleDiff > -365) return 'counterclockwise'        
        else return 'idle' // should beimpossible execute this else branch
    }    
}

// action to move closer to the target (in a way)
const getMoveActionTowardsTarget = (myX, myY, targetX, targetY) => {
    dx = myX - targetX
    dy = myY - targetY
    var {angle: targetAngle, distance: targetDistance} = coordsToPolar(dx, dy)
    const amIThere = targetDistance < parameters.tankSize + parameters.movePerFrame
    if (amIThere) {
        return 'idle'
    } else {
        if (Math.abs(dx) >= Math.abs(dy)) {
            if (dx >= 0) return 'left'
            else  return 'right'
        } else {
            if (dy >= 0) return 'up'
            else return 'down'
        }
    }
}

// returns 'undefined' as closestEnemy if no enemy found
const getClosestEnemyAndDistance = (myX, myY, enemies) => {
    var minDistance = Number.MAX_SAFE_INTEGER
    var closestEnemy = undefined;
    enemies.forEach(enemyTank => {                        
        const distance = (Math.abs(myX - enemyTank.x) + Math.abs(myY - enemyTank.y));
        if (distance < minDistance) {                
            minDistance = distance;
            closestEnemy = enemyTank
        }
    })
    return {
        closestEnemy,
        closestEnemyDistance: minDistance
    }
}
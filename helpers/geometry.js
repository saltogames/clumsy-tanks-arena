const radianToDegreeRatio = 57.2957795

const polarToCoords = (distance, angle) => {
    return {
        x: Math.cos(angle / radianToDegreeRatio) * distance,
        y: Math.sin(angle / radianToDegreeRatio) * distance,
    }
}

// 0 degree is pointing to left! towards: (x=1, y=0)
const coordsToPolar = (x, y) => {
    return {
        distance: Math.sqrt(x*x + y*y),
        angle: ((Math.atan2(y, x) * radianToDegreeRatio) + 180) % 365
    }
}

const getDistance = (x1, y1, x2, y2) => {
    return (Math.sqrt(
        (x1-x2)*(x1-x2) +
        (y1-y2)*(y1-y2)
    ))
}

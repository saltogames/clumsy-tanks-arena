const squaredanceJohnatan = {
    name: "Squaredance Johnatan",
    author: 'szemi',
    color: 'purple',
    step: (
        turnCount,
        myX,
        myY,
        myGunAngle,
        enemies, // x, y, gunAngle
        bullets // x,y, travelAngle
        // TODO: walls?
    ) => {   
        
        // 'circleing' in a square
        const squareSideSize = 70
        const moduloTurnCount = turnCount % (squareSideSize * 4)
        moveAction = 'idle'
        if (moduloTurnCount < squareSideSize) moveAction = 'up'
        if (squareSideSize <= moduloTurnCount && moduloTurnCount < 2 * squareSideSize) moveAction = 'left'
        if (2 * squareSideSize <= moduloTurnCount && moduloTurnCount < 3 * squareSideSize) moveAction = 'down'
        if (3 * squareSideSize <= moduloTurnCount) moveAction = 'right'
        
        
        // berserker:
        // - turn the gun towards the closest enemy (in the shortest way)
        // - always fire        
        var gunTurnAction = 'idle'        
        const {closestEnemy, closestEnemyDistance} = getClosestEnemyAndDistance(myX, myY, enemies)
        if (closestEnemy !== undefined) {                        
            var {distance: closestDistance, angle: closestEnemyAngle} = coordsToPolar(myX - closestEnemy.x, myY - closestEnemy.y)
            gunTurnAction = getTurnActionTowardsTargetAngle(myGunAngle, closestEnemyAngle)             
        }                        
        return {
            moveAction: moveAction,
            gunTurnAction: gunTurnAction,
            tryToFire: true 
        }
    }
}
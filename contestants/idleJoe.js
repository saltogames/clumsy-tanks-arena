const idleJoe = {
    name: "Idle Joe",
    author: 'szemi',
    color: 'gray',
    step: (
        turnCount, 
        myX,
        myY,
        myGunAngle,
        enemies, // {x, y, gunAngle}
        bullets, // {x, y, moveAngle}
        // TODO: walls?
    ) => {
        // does nothing, and always returns an 'idle' action (no action)
        return {
            moveAction: 'idle',  // 'idle', 'up', 'down', 'left', right
            gunTurnAction: 'idle',     // 'idle', 'clockwise', 'counterclockwise'
            tryToFire: false     // true or false
        }
    }
}
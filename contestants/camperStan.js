const camperStan = {
    name: "Camper Stan",
    author: 'szemi',
    color: 'green',
    step: (
        turnCount, 
        myX,
        myY,
        myGunAngle,
        enemies, // {x, y, gunAngle}
        bullets, // {x, y, moveAngle}
        // TODO: walls?
    ) => {
        // camper: goes to target location (0,0), and stays there
        /*var target = {
            x: myX >= constants.arenaWidth / 2 ? constants.arenaWidth : 0,
            y: myY >= constants.arenaHeight / 2 ? constants.arenaHeight : 0,
        }*/
        var target = {x: 0, y: 0}        
        var moveAction = getMoveActionTowardsTarget(myX, myY, target.x, target.y)                               
        var gunTurnAction = 'idle'     
        if (moveAction === 'idle'){
            // i'm arrived to the target
            // continuously sweeping gun between 0 and 90 degrees and always firing
            // (remember 0 degree points to 3 o'clock)
            var desiredSweepingAngle = Math.abs((turnCount % 90) - 45) * 2 // this will produce a : 0, 1, 2, 89, 90, 90, 89, ... 0 loop
            gunTurnAction = getTurnActionTowardsTargetAngle(myGunAngle, desiredSweepingAngle)
        } else {
            // i'm moving: fire backwards
            var {angle: targetAngle, distance} = coordsToPolar(myX - target.x, myY - target.y)
            var desiredBackwardsAngle = (targetAngle + 180) % 365
            gunTurnAction = getTurnActionTowardsTargetAngle(myGunAngle, desiredBackwardsAngle)
        }

        var tryToFire = true

        return {moveAction, gunTurnAction, tryToFire}
    }
}
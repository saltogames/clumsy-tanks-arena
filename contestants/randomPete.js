const randomPete = {
    name: "Random Pete",
    author: 'szemi',
    color: 'yellow',
    step: (
        turnCount,
        myX,
        myY,
        myGunAngle,
        enemies, // x, y, gunAngle
        bullets // x,y, travelAngle
        // TODO: walls?
    ) => {   
        
        // random movement and angle change every turn
        var moveAction = 'idle'
        const moveRandom = Math.random() * 4        
        if (moveRandom > 0 && moveRandom < 1) moveAction = 'up'
        else if (moveRandom > 1 && moveRandom < 2) moveAction = 'down'
        else if (moveRandom > 2 && moveRandom < 3) moveAction = 'left'
        else if (moveRandom > 3 && moveRandom < 4) moveAction = 'right'
        else moveAction = 'idle'
        
        var gunTurnAction = 'idle'        
        if (Math.random() > 0.5) gunTurnAction = 'clockwise'
        else gunTurnAction = 'counterclockwise'

        const tryToFire = Math.random() > 0.5 ? true : false        
        
        return {
            moveAction: moveAction,
            gunTurnAction: gunTurnAction,
            tryToFire: tryToFire 
        }
    }
}
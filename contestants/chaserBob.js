const chaserBob = {
    name: "Chaser Bob",
    author: 'szemi',
    color: 'red',
    step: (
        turnCount,
        myX,
        myY,
        myGunAngle,
        enemies,
        bullets,
        // TODO: walls?
    ) => {        
        // chaser:
        // if a target in sight: chase it
        // if more: chase the closest
        // if no target in sight: idle
        // if too close: 'idle'                                   
        var {closestEnemy, closestEnemyDistance} = getClosestEnemyAndDistance(myX, myY, enemies)
        var moveAction = getMoveActionTowardsTarget(myX, myY, closestEnemy.x, closestEnemy.y)
        
        
        if (closestEnemy !== undefined) {            
            closestEnemyDx = myX - closestEnemy.x
            closestEnemyDy = myY - closestEnemy.y
            if (closestEnemyDistance < 100) {
                moveAction = 'idle'
            } else {
                if (Math.abs(closestEnemyDx) >= Math.abs(closestEnemyDy)) {
                    if (closestEnemyDx >= 0) moveAction = 'left'
                    else moveAction = 'right'
                } else {
                    if (closestEnemyDy >= 0) moveAction = 'up'
                    else moveAction = 'down'
                }
            }            
        }
        // sniper:
        // - turn the gun towards the closest enemy (in the shortest way)
        // - only fire when aiming on target
        var gunTurnAction = 'idle'
        var tryTofire = false
        if (closestEnemy !== undefined){            
            var {distance, angle: closestEnemyAngle} = coordsToPolar(closestEnemyDx, closestEnemyDy)               
            gunTurnAction = getTurnActionTowardsTargetAngle(myGunAngle, closestEnemyAngle)
            tryToFire = Math.abs(myGunAngle - closestEnemyAngle) < 2
        }
        return {
            moveAction,
            gunTurnAction,
            tryToFire
        }
    }
}
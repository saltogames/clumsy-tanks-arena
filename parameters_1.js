const parameters = {
    // simulation
    stepInterval: 16, // ms (that means ~60fps if the processor can make it)    

    // tanks
    gunLength: 8, // px (when the gunlength is smaller than tankSize / 2 the tank will shoot itself)
    tankSize: 12, // px 
    gunTurnPerFrame: 3, // degree
    movePerFrame: 2,  // px 
    viewRadius: 100, // px (yet unused)

    // bullets
    bulletMovePerFrame: 4, // px
    gunCoolDown: 30,  // frames (0.5 sec at 60 fps)    
    bulletSize: 8,
    
}